from django.shortcuts import redirect, render, get_object_or_404
from .models import TodoList, TodoItem
from todos.forms import TodoItemForm, TodoListForm
from django.urls import reverse
#New addition to block.html below
from django.db.models import Q
from .models import TodoItem

# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists
    }

    return render(request, 'todos/todo_list_list.html', context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    # todo_items = TodoItem.objects.filter(list=todo_list) #commented for below functions

    # Get upcoming tasks (not completed) sorted by due date
    upcoming_tasks = todo_list.items.filter(is_completed=False).order_by('due_date')

    # Get completed tasks sorted by completion date (if needed)
    completed_tasks = todo_list.items.filter(is_completed=True).order_by('is_completed')

    context = {
        'todo_list': todo_list,
        'upcoming_tasks': upcoming_tasks,
        'completed_tasks': completed_tasks,
    }
    return render(request, 'todos/todo_list_detail.html', context)



def todo_list_create(request):
    if request.method == "POST":
        # We should use the form to validate the values
        #  and save them to the database
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()

            # If all goes well, we can redirect the browser
            #   to another page and leave the function
            return redirect("todo_list_detail", id=list.id)
    else:
        #Create an instance of the Django model form class
        form = TodoListForm()
    #Put the form in the context
    context = {
        "form": form, #added recipe
    }
    # Render the HTML template with the form
    return render(request, "todos/todo_list_form.html", context)



def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)




def todo_list_delete(request, id):
  todo_list = get_object_or_404(TodoList, id=id)
  if request.method == "POST":
    todo_list.delete()
    return redirect("todo_list_list")

  return render(request, "todos/todo_list_delete.html")



def todo_item_create(request):
    # todo_list = get_object_or_404(TodoList, id=list_id)
    # todo_list = TodoList.objects.all()

    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
           todo_item = form.save()
           return redirect('todo_list_detail', id=todo_item.list.id)
    else:
        form = TodoItemForm()

    context = {

        "form" : form,
    }
    return render(request, 'todos/todo_item_create.html', context)



def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)

    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_update.html", context)




#New addition to base.html below


def todo_item_delete(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        todo_item.delete()
        return redirect('todo_list_detail', id=todo_item.list.id)
    return render(request, 'todos/todo_item_delete.html', {'todo_item': todo_item})




def todo_item_search(request):
    query = request.GET.get('search')
    if query:
        todo_items = TodoItem.objects.filter(Q(task__icontains=query) | Q(category__icontains=query))
    else:
        todo_items = TodoItem.objects.none()

    context = {
        'todo_items': todo_items
    }
    return render(request, 'todos/todo_item_search.html', context)




# def test_template_render(request):
#     return render(request, 'todos/todo_item_search.html')
