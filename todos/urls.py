from django.urls import path
from .views import todo_item_delete, todo_item_search, todo_item_update, todo_list_list, todo_list_detail, todo_list_create, todo_list_update, todo_list_delete, todo_item_create


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    # path("todos/<int:id>/", todo_list_list, name="todo_list_detail"),#changed detail to list
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
    #New addition to base.html below
    path("items/<int:id>/delete/", todo_item_delete, name="todo_item_delete"),  # Add this path
    path("items/search/", todo_item_search, name="todo_item_search"),  # Add this path

]
