from django.db import models

# Create your models here.
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

# class TodoItem(models.Model):
#     task = models.CharField(max_length=100)
#     due_date = models.DateTimeField(null=True, blank=True)
#     is_completed = models.BooleanField(default=False)
#     list = models.ForeignKey(TodoList, on_delete=models.CASCADE, related_name="items")

#     def __str__(self):
#         return self.task


#New addition to base.html below and commented the one above.

class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(TodoList, on_delete=models.CASCADE, related_name="items")
    category = models.CharField(max_length=50, null=True, blank=True)  # Add category field

    def __str__(self):
        return self.task
