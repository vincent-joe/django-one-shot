from django import forms
from django.forms import ModelForm
from todos.models import TodoItem, TodoList

class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


# class TodoItemForm(ModelForm):
#     class Meta:
#         model = TodoItem
#         # Include the 'list' field in the form
#         fields = ["task", "due_date", "is_completed", "list"]

#     def __init__(self, *args, **kwargs):
#         super(TodoItemForm, self).__init__(*args, **kwargs)
#         # Customize the 'list' field to use a dropdown
#         self.fields['list'].widget = forms.Select(
#             choices=TodoList.objects.all().values_list('id', 'name'))
#         self.fields['is_completed'].widget = forms.CheckboxInput()
#         self.fields['due_date'].widget = forms.DateInput(
#             attrs={'type': 'date'})


#New addition to base.html below/commented the one above

class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list", "category"]

    def __init__(self, *args, **kwargs):
        super(TodoItemForm, self).__init__(*args, **kwargs)
        self.fields['list'].widget = forms.Select(choices=TodoList.objects.all().values_list('id', 'name'))
        self.fields['is_completed'].widget = forms.CheckboxInput()
        self.fields['due_date'].widget = forms.DateInput(attrs={'type': 'date'})
